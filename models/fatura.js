const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var SchemaModel = new Schema(
  {
    faturaNo            : String, // EFatura uretiyor
    belgeOid            : String,
    eFaturaDurum        : String,
    alimTarihi          : String,
    ettn                : String,
    olusturulmaTarihi   : String
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

module.exports =
  mongoose.models.Fatura || mongoose.model("Fatura", SchemaModel);
