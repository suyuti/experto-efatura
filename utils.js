const soap = require("soap");

const CreateClient = (url, user, password) => {
  return new Promise((resolve, reject) => {
    const wsSecurity = new soap.WSSecurity(user, password, {})
    soap.createClient(url, (err, client) => {
      if (err) {
        reject();
      } else {
        client.setSecurity(wsSecurity);

        resolve(client);
      }
    });
  });
};


module.exports = {
    CreateClient
}