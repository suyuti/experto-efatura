const Fatura = require('../fatura')
const Utils = require('../utils/ubl')


const faturaGonder1 = async (fatura) => {
    Fatura.faturaGonder(fatura)
}


var fatura = {
    no: 'EXP2020000000002',
    uuid: uuidv4(),
    duzenlemeTarihi: '2020-11-01',
    gonderen: {
        vkn: '3810685526',
        firmaAdi: 'Gonderen',
        adres: 'adres',
        ilce: 'ilce',
        il: 'il',
        ulke: 'ulke'
    },
    alan: {
        vkn: '3810685527',
        firmaAdi: 'Alan',
        webSitesi: 'www.alan',
        adres: 'adres',
        ilce: 'ilce',
        il: 'il',
        ulke: 'Turkiye',
    },
    kalemler: [
        {
            adet: 1,
            fiyat: 10,
            adi: 'A'
        }
    ],
    kdv: 1,
    araToplam: 2,
    vergiMatrahi: 3,
    genelToplam: 4,
}

let ubl = Utils.makeUbl(fatura)
console.log(ubl)

Fatura.faturaGonder(fatura)

//faturaGonder1({musteri: '12345'})
