process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

const Fatura = require("./models/fatura");
const { CreateClient } = require("./utils");
const {
  eFaturaKullanicisi,
  faturaNoUret,
} = require("./webmethods/eFaturaKullanicisi");
const {
  belgeGonder,
  gidenBelgeDurumSorgula,
} = require("./webmethods/belgeGonder");
const Utils = require("./utils/ubl");

const USER = process.env.USER;
const PASSWORD = process.env.PASSWORD;
const ENDPOINT = process.env.EFATURA_SERVICE_URL;

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

const faturaGonder = async (fatura) => {
  let client = await CreateClient(ENDPOINT, USER, PASSWORD);
  let vkn = fatura.alan.vkn; // '3810685526'
  let eFaturaKullanicisiMi = await eFaturaKullanicisi(client, vkn);
  if (eFaturaKullanicisiMi) {
    let faturaNo = await faturaNoUret(client, fatura.gonderen.vkn, "EXP");
    fatura.no = faturaNo;
    let ubl = Utils.makeUbl(fatura);
    let { belgeOid } = await belgeGonder(client, fatura);
    console.log(belgeOid);

    await Fatura.findByIdAndUpdate(fatura._id, {
      $set: { belgeOid: belgeOid, faturaNo: fatura.no },
    });

    var tekrarSorgula = true;
    var i = 1;
    do {
      console.log(i);
      i++;
      let durumKodu = await gidenBelgeDurumSorgula(
        client,
        fatura.gonderen.vkn,
        belgeOid
      );
      console.log(durumKodu);
      if (durumKodu.durum == 1) {
        // alindi. Schema kontrolu bekliyor. Tekrar sorulmali
        tekrarSorgula = true;
        await sleep(3000);
      } else if (durumKodu.durum == 2) {
        // fatura isleme hatasi
        // fatura uuid ve yerel belge no degistirilerek tekrar gonderilmeli
        console.log(durumKodu);
        tekrarSorgula = false; // TODO
      } else if (durumKodu.durum == 3) {
        tekrarSorgula = false;

        await Fatura.findByIdAndUpdate(fatura._id, {
          $set: {
            ettn: durumKodu.ettn,
            alimTarihi: durumKodu.alimTarihi,
            olusturulmaTarihi: durumKodu.olusturulmaTarihi,
          },
        });

        // fatura basarili islenmis
        if (durumKodu.gonderimDurumu === 2) {
          // GIB'e gonderilmis
        } else if (durumKodu.gonderimDurumu === 3) {
          // GIB'den yanit geldi.
          if (durumKodu.gonderimCevabiKodu === 1300) {
            if (durumKodu.gonderimDurumu === 4) {
              // islem basarili
            }
          } else if (durumKodu.gonderimCevabiKodu === 1200) {
          } else if (
            durumKodu.gonderimCevabiKodu > 1100 &&
            durumKodu.gonderimCevabiKodu < 1200
          ) {
            // belgeleri Tekrar Gonder
          }
        } else if (durumKodu.gonderimDurumu === 4) {
          // Alici basari ile almis
          if (durumKodu.yanitDurumu === -1) {
            // Temel fatura. Alicidan yanit beklenmiyor
          } else if (durumKodu.yanitDurumu === 0) {
            // Alicidan yanit bekleniyor
          } else if (durumKodu.yanitDurumu === 1) {
            // Red
          } else if (durumKodu.yanitDurumu === 2) {
            // Kabul
          }
        }
      }
    } while (tekrarSorgula);
  }
};

module.exports = {
  faturaGonder,
};
