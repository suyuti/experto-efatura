const makeUbl = (fatura) => {
  /*
        Zorunlu alanlar
        1. UBL Genisletme alani
        2. UBLVersionID
        3. CustomizationID
        4. ProfileID
        5. ID
        6. CopyIndicator
        7. UUID
        8. IssueDate
        9. InvoiceTypeCode
        10. DocumentCurrencyCode
        11. LineCountNumeric
        12. Signature
        13. AccountingSupplierParty
        14. AccountingCustomerParty
        15. TaxTotal
        16. LegalMonetaryTotal
        17. InvoiceLine


        fatura
        1. fatura bilgileri
            no: 3 alpha + 13 (YYYY - sira)
            uuid
            duzenlemeTarihi: YYYY-AA-GG
        2. gonderen firma
        3. alan firma
        4. satirlar
        5. toplamlar
    */
  var accountingSupplierParty = `<cac:AccountingSupplierParty>
            <cac:Party>
                <cac:PartyIdentification>
                    <cbc:ID schemeID="VKN">${fatura.gonderen.vkn}</cbc:ID>
                </cac:PartyIdentification>
                <cac:PartyName>
                    <cbc:Name>${fatura.gonderen.firmaAdi}</cbc:Name>
                </cac:PartyName>
                <cac:PostalAddress>
                    <cbc:StreetName>${fatura.gonderen.adres}</cbc:StreetName>
                    <cbc:CitySubdivisionName>${fatura.gonderen.ilce}</cbc:CitySubdivisionName>
                    <cbc:CityName>${fatura.gonderen.il}</cbc:CityName>
                    <cac:Country>
                        <cbc:Name>${fatura.gonderen.ulke}</cbc:Name>
                    </cac:Country>
                </cac:PostalAddress>
            </cac:Party>
        </cac:AccountingSupplierParty>`;

  var accountingCustomerParty = `
    <cac:AccountingCustomerParty>
    <cac:Party>
    <cac:PartyIdentification>
        <cbc:ID schemeID="VKN">${fatura.alan.vkn}</cbc:ID>
    </cac:PartyIdentification>
    <cac:PartyName>
    <cbc:Name>${fatura.alan.firmaAdi}</cbc:Name>
    </cac:PartyName>
    <cac:PostalAddress>
    <cbc:StreetName>${fatura.alan.adres}</cbc:StreetName>
    <cbc:CitySubdivisionName>${fatura.alan.ilce}</cbc:CitySubdivisionName>
    <cbc:CityName>${fatura.alan.il}</cbc:CityName>
    <cac:Country>
    <cbc:Name>${fatura.alan.ulke}</cbc:Name>
    </cac:Country>
    </cac:PostalAddress>
    </cac:Party>
    </cac:AccountingCustomerParty>`;

  var taxTotal = `
    <cac:TaxTotal>
        <cbc:TaxAmount currencyID="TRY">${fatura.kdv}</cbc:TaxAmount>
        <cac:TaxSubtotal>
            <cbc:TaxAmount currencyID="TRY">${fatura.kdv}</cbc:TaxAmount>
            <cac:TaxCategory>
                <cac:TaxScheme>
                    <cbc:TaxTypeCode>0015</cbc:TaxTypeCode>
                </cac:TaxScheme>
            </cac:TaxCategory>
        </cac:TaxSubtotal>
    </cac:TaxTotal>`;

  var legalMonetaryTotal = `<cac:LegalMonetaryTotal>
        <cbc:LineExtensionAmount currencyID="TRY">${
          fatura.araToplam
        }</cbc:LineExtensionAmount>
        <cbc:TaxExclusiveAmount currencyID="TRY">${
          fatura.vergiMatrahi
        }</cbc:TaxExclusiveAmount>
        <cbc:TaxInclusiveAmount currencyID="TRY">${
          fatura.araToplam + fatura.vergiMatrahi
        }</cbc:TaxInclusiveAmount>
        <cbc:PayableAmount currencyID="TRY">${
          fatura.genelToplam
        }</cbc:PayableAmount>
    </cac:LegalMonetaryTotal>`;

  var signature = `<cac:Signature>
        <cbc:ID schemeID="VKN_TCKN">${fatura.gonderen.vkn}</cbc:ID>
        <cac:SignatoryParty>
            <cac:PartyIdentification>
                <cbc:ID schemeID="VKN">${fatura.gonderen.vkn}</cbc:ID>
            </cac:PartyIdentification>
            <cac:PostalAddress>
                <cbc:StreetName>${fatura.gonderen.adres}</cbc:StreetName>
                <cbc:CitySubdivisionName>${fatura.gonderen.ilce}</cbc:CitySubdivisionName>
                <cbc:CityName>${fatura.gonderen.il}</cbc:CityName>
                <cac:Country>
                    <cbc:Name>${fatura.gonderen.ulke}</cbc:Name>
                </cac:Country>
            </cac:PostalAddress>
        </cac:SignatoryParty>
        <cac:DigitalSignatureAttachment>
            <cac:ExternalReference>
                <cbc:URI>#Signature</cbc:URI>
            </cac:ExternalReference>
        </cac:DigitalSignatureAttachment>
    </cac:Signature>`;

        var lines = ''
        var i = 1
    for(var k of fatura.kalemler) {
        lines = lines + `<cac:InvoiceLine>
        <cbc:ID>${i}</cbc:ID>
        <cbc:InvoicedQuantity unitCode="C62">${k.adet}</cbc:InvoicedQuantity>
        <cbc:LineExtensionAmount currencyID="TRY">${k.fiyat}</cbc:LineExtensionAmount>
        <cac:Item>
            <cbc:Name>${k.adi}</cbc:Name>
        </cac:Item>
        <cac:Price>
            <cbc:PriceAmount currencyID="TRY">${k.fiyat}</cbc:PriceAmount>
        </cac:Price>
    </cac:InvoiceLine>\n`
    i++
    }
/*
  let lines = fatura.kalemler.map((k, i) => {
    return `
    <cac:InvoiceLine>
        <cbc:ID>${i+1}</cbc:ID>
        <cbc:InvoicedQuantity unitCode="C62">${k.adet}</cbc:InvoicedQuantity>
        <cbc:LineExtensionAmount currencyID="TRY">${k.fiyat}</cbc:LineExtensionAmount>
        <cac:Item>
            <cbc:Name>${k.adi}</cbc:Name>
        </cac:Item>
        <cac:Price>
            <cbc:PriceAmount currencyID="TRY">${k.fiyat}</cbc:PriceAmount>
        </cac:Price>
    </cac:InvoiceLine>`;
  });
*/
  console.log(lines)




  var _ubl = [
    `<?xml version="1.0" encoding="UTF-8"?>
    <?xml-stylesheet type="text/xsl" href="general.xslt"?>
        <Invoice xsi:schemaLocation="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2 UBL-Invoice-2.1.xsd" xmlns="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2" xmlns:n4="http://www.altova.com/samplexml/other-namespace" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2">
            <ext:UBLExtensions>
                <ext:UBLExtension>
                    <ext:ExtensionContent>
                        <n4:auto-generated_for_wildcard/>
                    </ext:ExtensionContent>
                </ext:UBLExtension>
            </ext:UBLExtensions>
            <cbc:UBLVersionID>2.1</cbc:UBLVersionID>
            <cbc:CustomizationID>TR1.2</cbc:CustomizationID>
            <cbc:ProfileID>TEMELFATURA</cbc:ProfileID>
            <cbc:ID>${fatura.no}</cbc:ID>
            <cbc:CopyIndicator>false</cbc:CopyIndicator>
            <cbc:UUID>${fatura.uuid}</cbc:UUID>
            <cbc:IssueDate>${fatura.duzenlemeTarihi}</cbc:IssueDate>
            <cbc:InvoiceTypeCode>SATIS</cbc:InvoiceTypeCode>
            <cbc:DocumentCurrencyCode>TRY</cbc:DocumentCurrencyCode>
            <cbc:LineCountNumeric>${fatura.kalemler.length}</cbc:LineCountNumeric>
            
            <cac:DespatchDocumentReference>
                <cbc:ID>180921</cbc:ID>
                <cbc:IssueDate>2009-01-02</cbc:IssueDate>
            </cac:DespatchDocumentReference>`,
    signature,
    accountingSupplierParty,
    accountingCustomerParty,
    taxTotal,
    legalMonetaryTotal,
    lines,
    `</Invoice>`,
  ];
  return _ubl.join("\n");
};

module.exports = {
  makeUbl,
};
