const soap = require('soap')
const md5 = require('md5')
const { v4: uuidv4 } = require("uuid");
const { CreateClient } = require('../utils')
const Utils = require('../utils/ubl')
const fs = require('fs')

const USER = process.env.USER;
const PASSWORD = process.env.PASSWORD;
const ENDPOINT = process.env.EFATURA_SERVICE_URL;
const LOG_FOLDER = process.env.LOG_FOLDER;


const ubbl = `<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="general.xslt"?>
<Invoice xsi:schemaLocation="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2 UBL-Invoice-2.1.xsd" xmlns="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2" xmlns:n4="http://www.altova.com/samplexml/other-namespace" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2">
	<ext:UBLExtensions>
		<ext:UBLExtension>
			<ext:ExtensionContent>
				<n4:auto-generated_for_wildcard/>
			</ext:ExtensionContent>
		</ext:UBLExtension>
	</ext:UBLExtensions>
	<cbc:UBLVersionID>2.1</cbc:UBLVersionID>
	<cbc:CustomizationID>TR1.2</cbc:CustomizationID>
	<cbc:ProfileID>TEMELFATURA</cbc:ProfileID>
	<cbc:ID>EXP2020000000001</cbc:ID>
	<cbc:CopyIndicator>false</cbc:CopyIndicator>
	<cbc:UUID>F47AC10B-58CC-4372-A567-0E02B2C3D470</cbc:UUID>
	<cbc:IssueDate>2020-10-30</cbc:IssueDate>
	<cbc:InvoiceTypeCode>SATIS</cbc:InvoiceTypeCode>
	<cbc:DocumentCurrencyCode>TRY</cbc:DocumentCurrencyCode>
	<cbc:LineCountNumeric>1</cbc:LineCountNumeric>
	<cac:Signature>
		<cbc:ID schemeID="VKN_TCKN">3810685526</cbc:ID>
		<cac:SignatoryParty>
			<cac:PartyIdentification>
				<cbc:ID schemeID="VKN">3810685526</cbc:ID>
			</cac:PartyIdentification>
			<cac:PostalAddress>
				<cbc:StreetName>Papatya Caddesi Yasemin Sokak</cbc:StreetName>
				<cbc:BuildingNumber>21</cbc:BuildingNumber>
				<cbc:CitySubdivisionName>Beşiktaş</cbc:CitySubdivisionName>
				<cbc:CityName>İstanbul</cbc:CityName>
				<cbc:PostalZone>34100</cbc:PostalZone>
				<cac:Country>
					<cbc:Name>Türkiye</cbc:Name>
				</cac:Country>
			</cac:PostalAddress>
		</cac:SignatoryParty>
		<cac:DigitalSignatureAttachment>
			<cac:ExternalReference>
				<cbc:URI>#Signature</cbc:URI>
			</cac:ExternalReference>
		</cac:DigitalSignatureAttachment>
	</cac:Signature>
	<cac:AccountingSupplierParty>
		<cac:Party>
			<cbc:WebsiteURI>http://www.aaa.com.tr/</cbc:WebsiteURI>
			<cac:PartyIdentification>
				<cbc:ID schemeID="VKN">3810685526</cbc:ID>
			</cac:PartyIdentification>
			<cac:PartyName>
				<cbc:Name>AAA Anonim Şirketi</cbc:Name>
			</cac:PartyName>
			<cac:PostalAddress>
				<cbc:ID>1234567890</cbc:ID>
				<cbc:StreetName>Papatya Caddesi Yasemin Sokak</cbc:StreetName>
				<cbc:BuildingNumber>21</cbc:BuildingNumber>
				<cbc:CitySubdivisionName>Beşiktaş</cbc:CitySubdivisionName>
				<cbc:CityName>İstanbul</cbc:CityName>
				<cbc:PostalZone>34100</cbc:PostalZone>
				<cac:Country>
					<cbc:Name>Türkiye</cbc:Name>
				</cac:Country>
			</cac:PostalAddress>
			<cac:PartyTaxScheme>
				<cac:TaxScheme>
					<cbc:Name>Büyük Mükellefler</cbc:Name>
				</cac:TaxScheme>
			</cac:PartyTaxScheme>
			<cac:Contact>
				<cbc:Telephone>(212) 925 51515</cbc:Telephone>
				<cbc:Telefax>(212) 925505015</cbc:Telefax>
				<cbc:ElectronicMail>aa@aaa.com.tr</cbc:ElectronicMail>
			</cac:Contact>
		</cac:Party>
	</cac:AccountingSupplierParty>
	<cac:AccountingCustomerParty>
		<cac:Party>
			<cbc:WebsiteURI/>
			<cac:PartyIdentification>
				<cbc:ID schemeID="VKN">3810685526</cbc:ID>
			</cac:PartyIdentification>
			<cac:PartyName>
				<cbc:Name>BBB Anonim Şirketi</cbc:Name>
			</cac:PartyName>
			<cac:PostalAddress>
				<cbc:ID>ATATÜRK MAH.</cbc:ID>
				<cbc:Room>1</cbc:Room>
				<cbc:StreetName>6. Sokak</cbc:StreetName>
				<cbc:BuildingNumber>1</cbc:BuildingNumber>
				<cbc:CitySubdivisionName>Beşiktaş</cbc:CitySubdivisionName>
				<cbc:CityName>İstanbul</cbc:CityName>
				<cbc:PostalZone>34100</cbc:PostalZone>
				<cac:Country>
					<cbc:Name>Türkiye</cbc:Name>
				</cac:Country>
			</cac:PostalAddress>
			<cac:Contact>
				<cbc:ElectronicMail>1234567890@mydn.com.tr</cbc:ElectronicMail>
			</cac:Contact>
			<cac:Person>
				<cbc:FirstName>Ali</cbc:FirstName>
				<cbc:FamilyName>YILMAZ</cbc:FamilyName>
			</cac:Person>
		</cac:Party>
	</cac:AccountingCustomerParty>
	<cac:PaymentTerms>
		<cbc:Note>BBB Bank Otomatik Ödeme</cbc:Note>
		<cbc:PaymentDueDate>2009-01-20</cbc:PaymentDueDate>
	</cac:PaymentTerms>
	<cac:TaxTotal>
		<cbc:TaxAmount currencyID="TRY">2.73</cbc:TaxAmount>
		<cac:TaxSubtotal>
			<cbc:TaxableAmount currencyID="TRY">15.15</cbc:TaxableAmount>
			<cbc:TaxAmount currencyID="TRY">2.73</cbc:TaxAmount>
			<cac:TaxCategory>
				<cac:TaxScheme>
					<cbc:TaxTypeCode>0015</cbc:TaxTypeCode>
				</cac:TaxScheme>
			</cac:TaxCategory>
		</cac:TaxSubtotal>
	</cac:TaxTotal>
	<cac:LegalMonetaryTotal>
		<cbc:LineExtensionAmount currencyID="TRY">15.15</cbc:LineExtensionAmount>
		<cbc:TaxExclusiveAmount currencyID="TRY">15.15</cbc:TaxExclusiveAmount>
		<cbc:TaxInclusiveAmount currencyID="TRY">17.88</cbc:TaxInclusiveAmount>
		<cbc:PayableAmount currencyID="TRY">17.88</cbc:PayableAmount>
	</cac:LegalMonetaryTotal>
	<cac:InvoiceLine>
		<cbc:ID>1</cbc:ID>
		<cbc:InvoicedQuantity unitCode="KWH">101</cbc:InvoicedQuantity>
		<cbc:LineExtensionAmount currencyID="TRY">15.15</cbc:LineExtensionAmount>
		<cac:AllowanceCharge>
			<cbc:ChargeIndicator>false</cbc:ChargeIndicator>
			<cbc:MultiplierFactorNumeric>0.0</cbc:MultiplierFactorNumeric>
			<cbc:Amount currencyID="TRY">0</cbc:Amount>
			<cbc:BaseAmount currencyID="TRY">15.15</cbc:BaseAmount>
		</cac:AllowanceCharge>
		<cac:TaxTotal>
			<cbc:TaxAmount currencyID="TRY">2.73</cbc:TaxAmount>
			<cac:TaxSubtotal>
				<cbc:TaxableAmount currencyID="TRY">15.15</cbc:TaxableAmount>
				<cbc:TaxAmount currencyID="TRY">2.73</cbc:TaxAmount>
				<cbc:Percent>18.0</cbc:Percent>
				<cac:TaxCategory>
					<cac:TaxScheme>
						<cbc:Name>KDV</cbc:Name>
						<cbc:TaxTypeCode>0015</cbc:TaxTypeCode>
					</cac:TaxScheme>
				</cac:TaxCategory>
			</cac:TaxSubtotal>
		</cac:TaxTotal>
		<cac:Item>
			<cbc:Name>Elektrik Tüketim Bedeli</cbc:Name>
		</cac:Item>
		<cac:Price>
			<cbc:PriceAmount currencyID="TRY">0.15</cbc:PriceAmount>
		</cac:Price>
	</cac:InvoiceLine>
</Invoice>`


const belgeGonderAsync = async (client, fatura) => {
    return new Promise((resolve, reject) => {
        let ubl = Utils.makeUbl(fatura)
        let ublBase64 = Buffer.from(ubl).toString('base64')
        let hash = md5(ubl)

        fs.writeFileSync(`${LOG_FOLDER}/${fatura.uuid}.xml`, ubl)
        
        client.belgeGonder({
            vergiTcKimlikNo: fatura.gonderen.vkn,
            belgeTuru: 'FATURA_UBL',
            belgeNo: fatura.uuid,
            veri: ublBase64,
            belgeHash : hash,
            mimeType: 'application/xml'
        }, (err, res) => {
            console.log(err)
            if (!err) {
                console.log(res)
                resolve(res)
            }
        })
    })
}

const belgeGonder = async (client, fatura) => {
    let r = await belgeGonderAsync(client, fatura)
    return r
    //let client = await CreateClient(ENDPOINT, USER, PASSWORD)
}

const gidenBelgeDurumSorgula = async (client, vkn, belgeOid) => {
    return new Promise((resolve, reject) => {
        client.gidenBelgeDurumSorgula({vergiTcKimlikNo: vkn, belgeOid: belgeOid}, (err, res) => {
            if (!err) {
                resolve(res.return)
            }
        })
    })
}

module.exports = {
    belgeGonder,
    gidenBelgeDurumSorgula
}