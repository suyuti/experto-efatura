const soap = require("soap");
const md5 = require("md5");
const { v4: uuidv4 } = require("uuid");
const { CreateClient } = require("../utils");

const USER = process.env.USER;
const PASSWORD = process.env.PASSWORD;
const ENDPOINT = process.env.EFATURA_SERVICE_URL;

const eFaturaKullanicisiAsync = async (client, vkn) => {
  return new Promise((resolve, reject) => {
    //console.log(client)
    console.log(vkn);
    client.efaturaKullaniciBilgisi({ vergiTcKimlikNo: vkn }, (err, res) => {
      console.log(err);
      console.log(res);
      if (!err) {
        resolve(res);
      }
    });
  });
};

const eFaturaKullanicisi = async (client, vkn) => {
  let result = await eFaturaKullanicisiAsync(client, vkn);
  return result !== null;
};

const faturaNoUret = async (client, vkn, faturaKodu) => {
  return new Promise((resolve, reject) => {
    client.faturaNoUret(
      { vknTckn: vkn, faturaKodu: faturaKodu },
      (err, res) => {
        if (!err) {
          resolve(res.return)
        }
      }
    );
  });
};

module.exports = {
  eFaturaKullanicisi,
  faturaNoUret,
};
