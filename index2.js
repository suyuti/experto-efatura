const axios = require("axios");
var parseString = require("xml2js").parseString;
var parser = require("xml2json");
var md5 = require("md5");
const { v4: uuidv4 } = require("uuid");
const ubl = require('./ubl1')
var fs = require('fs')
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

const Service =
  "https://erpefaturatest.cs.com.tr:8043/efatura/ws/connectorService?wsdl";

  const xmls = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.connector.uut.cs.com.tr/">
  <soapenv:Header>
  <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
  <wsse:UsernameToken>
  <wsse:Username>3810685526</wsse:Username>
  <wsse:Password>Experto2020</wsse:Password>
  </wsse:UsernameToken>
  </wsse:Security>
  </soapenv:Header>
  <soapenv:Body>
  <ser:efaturaKullaniciBilgisi>
  <!-- Optional: -->
  <vergiTcKimlikNo>1201830512</vergiTcKimlikNo>
  </ser:efaturaKullaniciBilgisi>
  </soapenv:Body>
  </soapenv:Envelope>`;

  const xmls2 = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.connector.uut.cs.com.tr/">
  <soapenv:Header>
  <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
  <wsse:UsernameToken>
  <wsse:Username>3810685526</wsse:Username>
  <wsse:Password>Experto2020</wsse:Password>
  </wsse:UsernameToken>
  </wsse:Security>
  </soapenv:Header>
  <soapenv:Body>
  <ser:faturaNoUret>
  <vknTckn>3810685526</vknTckn>
  <faturaKodu>A</faturaKodu>
  </ser:faturaNoUret>
  </soapenv:Body>
  </soapenv:Envelope>`;

  const xmls3 = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.connector.uut.cs.com.tr/">
  <soapenv:Header>
  <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
  <wsse:UsernameToken>
  <wsse:Username>3810685526</wsse:Username>
  <wsse:Password>Experto2020</wsse:Password>
  </wsse:UsernameToken>
  </wsse:Security>
  </soapenv:Header>
  <soapenv:Body>
  <ser:temelKontrollerIleBelgeGonder>
  <vergiTcKimlikNo>3810685526</vergiTcKimlikNo>
  <belgeTuru>FATURA_UBL</belgeTuru>
  <belgeNo>${uuidv4()}</belgeNo>
  <veri>${Buffer.from(ubl.ubl).toString("base64")}</veri>
  <belgeHash>${md5(ubl.ubl)}</belgeHash>
  <mimeType>application/xml</mimeType>
  <belgeVersiyon>1.0</belgeVersiyon>
  </ser:temelKontrollerIleBelgeGonder>
  </soapenv:Body>
  </soapenv:Envelope>`;
    
  const xmls4 = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.connector.uut.cs.com.tr/">
  <soapenv:Header> 
   <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"> 
   <wsse:UsernameToken>
  <wsse:Username>3810685526</wsse:Username>
  <wsse:Password>Experto2020</wsse:Password>
  </wsse:UsernameToken> 
   </wsse:Security>
  </soapenv:Header>
     <soapenv:Body>
        <ser:belgeGonder>
           <!--Optional:-->
           <vergiTcKimlikNo>3810685526</vergiTcKimlikNo>
           <!--Optional:-->
           <belgeTuru>FATURA_UBL</belgeTuru>
           <!--Optional:-->
           <belgeNo>45c10727-002a-43a1-81f4-13baf803c8c5</belgeNo>
           <!--Optional:-->
           <veri>PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPD94bWwtc3R5bGVzaGVldCB0eXBlPSJ0ZXh0L3hzbCIgaHJlZj0iZ2VuZXJhbC54c2x0Ij8+CjxJbnZvaWNlIHhzaTpzY2hlbWFMb2NhdGlvbj0idXJuOm9hc2lzOm5hbWVzOnNwZWNpZmljYXRpb246dWJsOnNjaGVtYTp4c2Q6SW52b2ljZS0yIFVCTC1JbnZvaWNlLTIuMS54c2QiIHhtbG5zPSJ1cm46b2FzaXM6bmFtZXM6c3BlY2lmaWNhdGlvbjp1Ymw6c2NoZW1hOnhzZDpJbnZvaWNlLTIiIHhtbG5zOm40PSJodHRwOi8vd3d3LmFsdG92YS5jb20vc2FtcGxleG1sL290aGVyLW5hbWVzcGFjZSIgeG1sbnM6eHNpPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYS1pbnN0YW5jZSIgeG1sbnM6Y2FjPSJ1cm46b2FzaXM6bmFtZXM6c3BlY2lmaWNhdGlvbjp1Ymw6c2NoZW1hOnhzZDpDb21tb25BZ2dyZWdhdGVDb21wb25lbnRzLTIiIHhtbG5zOmNiYz0idXJuOm9hc2lzOm5hbWVzOnNwZWNpZmljYXRpb246dWJsOnNjaGVtYTp4c2Q6Q29tbW9uQmFzaWNDb21wb25lbnRzLTIiIHhtbG5zOmV4dD0idXJuOm9hc2lzOm5hbWVzOnNwZWNpZmljYXRpb246dWJsOnNjaGVtYTp4c2Q6Q29tbW9uRXh0ZW5zaW9uQ29tcG9uZW50cy0yIj4KCTxleHQ6VUJMRXh0ZW5zaW9ucz4KCQk8ZXh0OlVCTEV4dGVuc2lvbj4KCQkJPGV4dDpFeHRlbnNpb25Db250ZW50PgoJCQkJPG40OmF1dG8tZ2VuZXJhdGVkX2Zvcl93aWxkY2FyZC8+CgkJCTwvZXh0OkV4dGVuc2lvbkNvbnRlbnQ+CgkJPC9leHQ6VUJMRXh0ZW5zaW9uPgoJPC9leHQ6VUJMRXh0ZW5zaW9ucz4KCTxjYmM6VUJMVmVyc2lvbklEPjIuMTwvY2JjOlVCTFZlcnNpb25JRD4KCTxjYmM6Q3VzdG9taXphdGlvbklEPlRSMS4yPC9jYmM6Q3VzdG9taXphdGlvbklEPgoJPGNiYzpQcm9maWxlSUQ+VEVNRUxGQVRVUkE8L2NiYzpQcm9maWxlSUQ+Cgk8Y2JjOklEPkdJQjIwMDkwMDAwMDAwMDAxPC9jYmM6SUQ+Cgk8Y2JjOkNvcHlJbmRpY2F0b3I+ZmFsc2U8L2NiYzpDb3B5SW5kaWNhdG9yPgoJPGNiYzpVVUlEPkY0N0FDMTBCLTU4Q0MtNDM3Mi1BNTY3LTBFMDJCMkMzRDQ3OTwvY2JjOlVVSUQ+Cgk8Y2JjOklzc3VlRGF0ZT4yMDA5LTAxLTA1PC9jYmM6SXNzdWVEYXRlPgoJPGNiYzpJc3N1ZVRpbWU+MTQ6NDI6MDA8L2NiYzpJc3N1ZVRpbWU+Cgk8Y2JjOkludm9pY2VUeXBlQ29kZT5TQVRJUzwvY2JjOkludm9pY2VUeXBlQ29kZT4KCTxjYmM6RG9jdW1lbnRDdXJyZW5jeUNvZGU+VFJZPC9jYmM6RG9jdW1lbnRDdXJyZW5jeUNvZGU+Cgk8Y2JjOkxpbmVDb3VudE51bWVyaWM+MTwvY2JjOkxpbmVDb3VudE51bWVyaWM+Cgk8Y2FjOkludm9pY2VQZXJpb2Q+CgkJPGNiYzpTdGFydERhdGU+MjAwOC0xMi0wNTwvY2JjOlN0YXJ0RGF0ZT4KCQk8Y2JjOkVuZERhdGU+MjAwOS0wMS0wNTwvY2JjOkVuZERhdGU+Cgk8L2NhYzpJbnZvaWNlUGVyaW9kPgoJPGNhYzpTaWduYXR1cmU+CgkJPGNiYzpJRCBzY2hlbWVJRD0iVktOX1RDS04iPjEyODgzMzE1MjE8L2NiYzpJRD4KCQk8Y2FjOlNpZ25hdG9yeVBhcnR5PgoJCQk8Y2FjOlBhcnR5SWRlbnRpZmljYXRpb24+CgkJCQk8Y2JjOklEIHNjaGVtZUlEPSJWS04iPjEyODgzMzE1MjE8L2NiYzpJRD4KCQkJPC9jYWM6UGFydHlJZGVudGlmaWNhdGlvbj4KCQkJPGNhYzpQb3N0YWxBZGRyZXNzPgoJCQkJPGNiYzpTdHJlZXROYW1lPlBhcGF0eWEgQ2FkZGVzaSBZYXNlbWluIFNva2FrPC9jYmM6U3RyZWV0TmFtZT4KCQkJCTxjYmM6QnVpbGRpbmdOdW1iZXI+MjE8L2NiYzpCdWlsZGluZ051bWJlcj4KCQkJCTxjYmM6Q2l0eVN1YmRpdmlzaW9uTmFtZT5CZcWfaWt0YcWfPC9jYmM6Q2l0eVN1YmRpdmlzaW9uTmFtZT4KCQkJCTxjYmM6Q2l0eU5hbWU+xLBzdGFuYnVsPC9jYmM6Q2l0eU5hbWU+CgkJCQk8Y2JjOlBvc3RhbFpvbmU+MzQxMDA8L2NiYzpQb3N0YWxab25lPgoJCQkJPGNhYzpDb3VudHJ5PgoJCQkJCTxjYmM6TmFtZT5Uw7xya2l5ZTwvY2JjOk5hbWU+CgkJCQk8L2NhYzpDb3VudHJ5PgoJCQk8L2NhYzpQb3N0YWxBZGRyZXNzPgoJCTwvY2FjOlNpZ25hdG9yeVBhcnR5PgoJCTxjYWM6RGlnaXRhbFNpZ25hdHVyZUF0dGFjaG1lbnQ+CgkJCTxjYWM6RXh0ZXJuYWxSZWZlcmVuY2U+CgkJCQk8Y2JjOlVSST4jU2lnbmF0dXJlPC9jYmM6VVJJPgoJCQk8L2NhYzpFeHRlcm5hbFJlZmVyZW5jZT4KCQk8L2NhYzpEaWdpdGFsU2lnbmF0dXJlQXR0YWNobWVudD4KCTwvY2FjOlNpZ25hdHVyZT4KCTxjYWM6QWNjb3VudGluZ1N1cHBsaWVyUGFydHk+CgkJPGNhYzpQYXJ0eT4KCQkJPGNiYzpXZWJzaXRlVVJJPmh0dHA6Ly93d3cuYWFhLmNvbS50ci88L2NiYzpXZWJzaXRlVVJJPgoJCQk8Y2FjOlBhcnR5SWRlbnRpZmljYXRpb24+CgkJCQk8Y2JjOklEIHNjaGVtZUlEPSJWS04iPjEyODgzMzE1MjE8L2NiYzpJRD4KCQkJPC9jYWM6UGFydHlJZGVudGlmaWNhdGlvbj4KCQkJPGNhYzpQYXJ0eU5hbWU+CgkJCQk8Y2JjOk5hbWU+QUFBIEFub25pbSDFnmlya2V0aTwvY2JjOk5hbWU+CgkJCTwvY2FjOlBhcnR5TmFtZT4KCQkJPGNhYzpQb3N0YWxBZGRyZXNzPgoJCQkJPGNiYzpJRD4xMjM0NTY3ODkwPC9jYmM6SUQ+CgkJCQk8Y2JjOlN0cmVldE5hbWU+UGFwYXR5YSBDYWRkZXNpIFlhc2VtaW4gU29rYWs8L2NiYzpTdHJlZXROYW1lPgoJCQkJPGNiYzpCdWlsZGluZ051bWJlcj4yMTwvY2JjOkJ1aWxkaW5nTnVtYmVyPgoJCQkJPGNiYzpDaXR5U3ViZGl2aXNpb25OYW1lPkJlxZ9pa3RhxZ88L2NiYzpDaXR5U3ViZGl2aXNpb25OYW1lPgoJCQkJPGNiYzpDaXR5TmFtZT7EsHN0YW5idWw8L2NiYzpDaXR5TmFtZT4KCQkJCTxjYmM6UG9zdGFsWm9uZT4zNDEwMDwvY2JjOlBvc3RhbFpvbmU+CgkJCQk8Y2FjOkNvdW50cnk+CgkJCQkJPGNiYzpOYW1lPlTDvHJraXllPC9jYmM6TmFtZT4KCQkJCTwvY2FjOkNvdW50cnk+CgkJCTwvY2FjOlBvc3RhbEFkZHJlc3M+CgkJCTxjYWM6UGFydHlUYXhTY2hlbWU+CgkJCQk8Y2FjOlRheFNjaGVtZT4KCQkJCQk8Y2JjOk5hbWU+QsO8ecO8ayBNw7xrZWxsZWZsZXI8L2NiYzpOYW1lPgoJCQkJPC9jYWM6VGF4U2NoZW1lPgoJCQk8L2NhYzpQYXJ0eVRheFNjaGVtZT4KCQkJPGNhYzpDb250YWN0PgoJCQkJPGNiYzpUZWxlcGhvbmU+KDIxMikgOTI1IDUxNTE1PC9jYmM6VGVsZXBob25lPgoJCQkJPGNiYzpUZWxlZmF4PigyMTIpIDkyNTUwNTAxNTwvY2JjOlRlbGVmYXg+CgkJCQk8Y2JjOkVsZWN0cm9uaWNNYWlsPmFhQGFhYS5jb20udHI8L2NiYzpFbGVjdHJvbmljTWFpbD4KCQkJPC9jYWM6Q29udGFjdD4KCQk8L2NhYzpQYXJ0eT4KCTwvY2FjOkFjY291bnRpbmdTdXBwbGllclBhcnR5PgoJPGNhYzpBY2NvdW50aW5nQ3VzdG9tZXJQYXJ0eT4KCQk8Y2FjOlBhcnR5PgoJCQk8Y2JjOldlYnNpdGVVUkkvPgoJCQk8Y2FjOlBhcnR5SWRlbnRpZmljYXRpb24+CgkJCQk8Y2JjOklEIHNjaGVtZUlEPSJUQ0tOIj4xMjM0NTY3ODkwPC9jYmM6SUQ+CgkJCTwvY2FjOlBhcnR5SWRlbnRpZmljYXRpb24+CgkJCTxjYWM6UGFydHlJZGVudGlmaWNhdGlvbj4KCQkJCTxjYmM6SUQgc2NoZW1lSUQ9IlRFU0lTQVROTyI+MTIzNDU2NzwvY2JjOklEPgoJCQk8L2NhYzpQYXJ0eUlkZW50aWZpY2F0aW9uPgoJCQk8Y2FjOlBhcnR5SWRlbnRpZmljYXRpb24+CgkJCQk8Y2JjOklEIHNjaGVtZUlEPSJTQVlBQ05PIj4xMjM0NTY3ODwvY2JjOklEPgoJCQk8L2NhYzpQYXJ0eUlkZW50aWZpY2F0aW9uPgoJCQk8Y2FjOlBvc3RhbEFkZHJlc3M+CgkJCQk8Y2JjOklEPkFUQVTDnFJLIE1BSC48L2NiYzpJRD4KCQkJCTxjYmM6Um9vbT4xPC9jYmM6Um9vbT4KCQkJCTxjYmM6U3RyZWV0TmFtZT42LiBTb2thazwvY2JjOlN0cmVldE5hbWU+CgkJCQk8Y2JjOkJ1aWxkaW5nTnVtYmVyPjE8L2NiYzpCdWlsZGluZ051bWJlcj4KCQkJCTxjYmM6Q2l0eVN1YmRpdmlzaW9uTmFtZT5CZcWfaWt0YcWfPC9jYmM6Q2l0eVN1YmRpdmlzaW9uTmFtZT4KCQkJCTxjYmM6Q2l0eU5hbWU+xLBzdGFuYnVsPC9jYmM6Q2l0eU5hbWU+CgkJCQk8Y2JjOlBvc3RhbFpvbmU+MzQxMDA8L2NiYzpQb3N0YWxab25lPgoJCQkJPGNhYzpDb3VudHJ5PgoJCQkJCTxjYmM6TmFtZT5Uw7xya2l5ZTwvY2JjOk5hbWU+CgkJCQk8L2NhYzpDb3VudHJ5PgoJCQk8L2NhYzpQb3N0YWxBZGRyZXNzPgoJCQk8Y2FjOkNvbnRhY3Q+CgkJCQk8Y2JjOkVsZWN0cm9uaWNNYWlsPjEyMzQ1Njc4OTBAbXlkbi5jb20udHI8L2NiYzpFbGVjdHJvbmljTWFpbD4KCQkJPC9jYWM6Q29udGFjdD4KCQkJPGNhYzpQZXJzb24+CgkJCQk8Y2JjOkZpcnN0TmFtZT5BbGk8L2NiYzpGaXJzdE5hbWU+CgkJCQk8Y2JjOkZhbWlseU5hbWU+WUlMTUFaPC9jYmM6RmFtaWx5TmFtZT4KCQkJPC9jYWM6UGVyc29uPgoJCTwvY2FjOlBhcnR5PgoJPC9jYWM6QWNjb3VudGluZ0N1c3RvbWVyUGFydHk+Cgk8Y2FjOlBheW1lbnRUZXJtcz4KCQk8Y2JjOk5vdGU+QkJCIEJhbmsgT3RvbWF0aWsgw5ZkZW1lPC9jYmM6Tm90ZT4KCQk8Y2JjOlBheW1lbnREdWVEYXRlPjIwMDktMDEtMjA8L2NiYzpQYXltZW50RHVlRGF0ZT4KCTwvY2FjOlBheW1lbnRUZXJtcz4KCTxjYWM6VGF4VG90YWw+CgkJPGNiYzpUYXhBbW91bnQgY3VycmVuY3lJRD0iVFJZIj4yLjczPC9jYmM6VGF4QW1vdW50PgoJCTxjYWM6VGF4U3VidG90YWw+CgkJCTxjYmM6VGF4YWJsZUFtb3VudCBjdXJyZW5jeUlEPSJUUlkiPjE1LjE1PC9jYmM6VGF4YWJsZUFtb3VudD4KCQkJPGNiYzpUYXhBbW91bnQgY3VycmVuY3lJRD0iVFJZIj4yLjczPC9jYmM6VGF4QW1vdW50PgoJCQk8Y2FjOlRheENhdGVnb3J5PgoJCQkJPGNhYzpUYXhTY2hlbWU+CgkJCQkJPGNiYzpUYXhUeXBlQ29kZT4wMDE1PC9jYmM6VGF4VHlwZUNvZGU+CgkJCQk8L2NhYzpUYXhTY2hlbWU+CgkJCTwvY2FjOlRheENhdGVnb3J5PgoJCTwvY2FjOlRheFN1YnRvdGFsPgoJPC9jYWM6VGF4VG90YWw+Cgk8Y2FjOkxlZ2FsTW9uZXRhcnlUb3RhbD4KCQk8Y2JjOkxpbmVFeHRlbnNpb25BbW91bnQgY3VycmVuY3lJRD0iVFJZIj4xNS4xNTwvY2JjOkxpbmVFeHRlbnNpb25BbW91bnQ+CgkJPGNiYzpUYXhFeGNsdXNpdmVBbW91bnQgY3VycmVuY3lJRD0iVFJZIj4xNS4xNTwvY2JjOlRheEV4Y2x1c2l2ZUFtb3VudD4KCQk8Y2JjOlRheEluY2x1c2l2ZUFtb3VudCBjdXJyZW5jeUlEPSJUUlkiPjE3Ljg4PC9jYmM6VGF4SW5jbHVzaXZlQW1vdW50PgoJCTxjYmM6UGF5YWJsZUFtb3VudCBjdXJyZW5jeUlEPSJUUlkiPjE3Ljg4PC9jYmM6UGF5YWJsZUFtb3VudD4KCTwvY2FjOkxlZ2FsTW9uZXRhcnlUb3RhbD4KCTxjYWM6SW52b2ljZUxpbmU+CgkJPGNiYzpJRD4xPC9jYmM6SUQ+CgkJPGNiYzpJbnZvaWNlZFF1YW50aXR5IHVuaXRDb2RlPSJLV0giPjEwMTwvY2JjOkludm9pY2VkUXVhbnRpdHk+CgkJPGNiYzpMaW5lRXh0ZW5zaW9uQW1vdW50IGN1cnJlbmN5SUQ9IlRSWSI+MTUuMTU8L2NiYzpMaW5lRXh0ZW5zaW9uQW1vdW50PgoJCTxjYWM6QWxsb3dhbmNlQ2hhcmdlPgoJCQk8Y2JjOkNoYXJnZUluZGljYXRvcj5mYWxzZTwvY2JjOkNoYXJnZUluZGljYXRvcj4KCQkJPGNiYzpNdWx0aXBsaWVyRmFjdG9yTnVtZXJpYz4wLjA8L2NiYzpNdWx0aXBsaWVyRmFjdG9yTnVtZXJpYz4KCQkJPGNiYzpBbW91bnQgY3VycmVuY3lJRD0iVFJZIj4wPC9jYmM6QW1vdW50PgoJCQk8Y2JjOkJhc2VBbW91bnQgY3VycmVuY3lJRD0iVFJZIj4xNS4xNTwvY2JjOkJhc2VBbW91bnQ+CgkJPC9jYWM6QWxsb3dhbmNlQ2hhcmdlPgoJCTxjYWM6VGF4VG90YWw+CgkJCTxjYmM6VGF4QW1vdW50IGN1cnJlbmN5SUQ9IlRSWSI+Mi43MzwvY2JjOlRheEFtb3VudD4KCQkJPGNhYzpUYXhTdWJ0b3RhbD4KCQkJCTxjYmM6VGF4YWJsZUFtb3VudCBjdXJyZW5jeUlEPSJUUlkiPjE1LjE1PC9jYmM6VGF4YWJsZUFtb3VudD4KCQkJCTxjYmM6VGF4QW1vdW50IGN1cnJlbmN5SUQ9IlRSWSI+Mi43MzwvY2JjOlRheEFtb3VudD4KCQkJCTxjYmM6UGVyY2VudD4xOC4wPC9jYmM6UGVyY2VudD4KCQkJCTxjYWM6VGF4Q2F0ZWdvcnk+CgkJCQkJPGNhYzpUYXhTY2hlbWU+CgkJCQkJCTxjYmM6TmFtZT5LRFY8L2NiYzpOYW1lPgoJCQkJCQk8Y2JjOlRheFR5cGVDb2RlPjAwMTU8L2NiYzpUYXhUeXBlQ29kZT4KCQkJCQk8L2NhYzpUYXhTY2hlbWU+CgkJCQk8L2NhYzpUYXhDYXRlZ29yeT4KCQkJPC9jYWM6VGF4U3VidG90YWw+CgkJPC9jYWM6VGF4VG90YWw+CgkJPGNhYzpJdGVtPgoJCQk8Y2JjOk5hbWU+RWxla3RyaWsgVMO8a2V0aW0gQmVkZWxpPC9jYmM6TmFtZT4KCQk8L2NhYzpJdGVtPgoJCTxjYWM6UHJpY2U+CgkJCTxjYmM6UHJpY2VBbW91bnQgY3VycmVuY3lJRD0iVFJZIj4wLjE1PC9jYmM6UHJpY2VBbW91bnQ+CgkJPC9jYWM6UHJpY2U+Cgk8L2NhYzpJbnZvaWNlTGluZT4KPC9JbnZvaWNlPg==</veri>
           <!--Optional:-->
           <belgeHash>c43c0236494844d722ddb12f4910150a</belgeHash>
           <!--Optional:-->
           <mimeType>application/xml</mimeType>
           <!--Optional:-->
           <belgeVersiyon>1.0</belgeVersiyon>
        </ser:belgeGonder>
     </soapenv:Body>
  </soapenv:Envelope>`

  console.log(xmls4)
  fs.writeFileSync('req4.xml', xmls3)
axios
  .post(Service, xmls4, { headers: { "Content-Type": "text/xml" } })
  .then((res) => {
    console.log(res.data);

    fs.writeFileSync('res4.xml', res.data)
    let f = parser.toJson(res.data);
    console.log("-------------------");
    console.log(f);
    console.log("-------------------");
    console.log(JSON.parse(f));
    let o = JSON.parse(f);
    console.log("-------------------");

    console.log(
      o["S:Envelope"]["S:Body"] //["ns2:efaturaKullaniciBilgisiResponse"].return
    );
    //console.log(o['S:Envelope']['S:Body']['ns2:efaturaKullaniciBilgisiResponse'].return)


    parseString(res.data, (e, r) => {
      console.log(
        r["S:Envelope"]["S:Body"][
          "ns2:efaturaKullaniciBilgisiResponse"
        ] /*['ns2:efaturaKullaniciBilgisiResponse']*/
      );
    });
  })
  .catch((e) => {
    fs.writeFileSync('resp4.xml', e.response.data)
    
    console.log(e.response.data)});
