const amqplib = require("amqplib/callback_api");
const amqp = process.env.AMQP_URL;
const fatura_queue = process.env.QUEUE_FATURAGONDER;
const moment = require('moment')
const Fatura = require('./fatura')
const initMongo = require("./config/mongo");

initMongo();


amqplib.connect(amqp, (err, connection) => {
    if (err) {
      console.error(err.stack);
      return process.exit(1);
    }
  
    connection.createChannel((err, channel) => {
      if (err) {
      }
      channel.assertQueue(fatura_queue, { durable: true }, (err) => {
        if (err) {
        }
        channel.prefetch(1);
        channel.consume(fatura_queue, (data) => {
          if (data === null) {
            return;
          }
          console.log("FATURA");
          let {fatura} = JSON.parse(data.content.toString());
          console.log(fatura)
          /*let f = {
            uuid : fatura.uuid,
            duzenlemeTarihi : moment(fatura.duzenlemeTarihi).format('YYYY-MM-DD'),
            gonderen: {
              vkn: '',
              firmaAdi: '',
              adres: '',
              ilce: '',
              il: '',
              ulke: 'Türkiye'
            },
            alan: {
              vkn: '',
              firmaAdi: fatura.musteriMarka,
              adres: '',
              ilce: '',
              il: '',
              ulke: 'Türkiye'
            },
            kdv: fatura.toplamKdv,
            araToplam: fatura.araToplam,
            vergiMatrahi: fatura.araToplam,
            genelToplam: fatura.genelToplam
          }
          console.log(f)
          */
          Fatura.faturaGonder(fatura)
          .then(() => {
            console.log('ok')
            return channel.ack(data)
          })
          
          /*Mail.sendMail(message.fromMsId, message.toMsId, message.mailObj).then(
            () => {
              return channel.ack(data);
            }
          );*/
        });
      });
    });
  });
  
